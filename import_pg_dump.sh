#!/bin/bash
# vim: set tabstop=4 shiftwidth=4 expandtab cindent:
# LICENSE: MIT, see LICENSE file

function usage 
{
    echo 'PostgreSQL automatic dump import'
    echo "Usage: $0 -d dbname [-r rolename] -s sqlfile [-c] [-e] [-p]"
    echo '    -d database name'
    echo '    -r role name (default: database name)'
    echo '    -s SQL dump file'
    echo '    -c enables latin1 to utf8 conversion'
    echo '    -e create DB with non default encoding (e.g. SQL_ASCII)'
    echo '    -p pretend'
    exit 1
}


pguser='postgres'
pretend=0
filter='cat'
while getopts "d:e:r:s:cp" Option
do
    case $Option in
        c ) filter='iconv -f latin1 -t utf8';;
        d ) dbname="$OPTARG";;
        e ) encoding="$OPTARG";;
        r ) role="$OPTARG";;
        s ) sqlfile="$OPTARG";;
        p ) pretend=1
    esac 
done

if [ "$dbname" = "" -o ! -f "$sqlfile" ]; then
    usage
fi

if [ "$role" = "" ]; then
    role=$dbname
fi

if [ $pretend -eq 0 ]; then
    echo "This will DROP database $dbname and reload it from $sqlfile"
    read -p "Are you sure? " -n 1
    if [[ ! $REPLY =~ ^[Yy]$ ]]
    then
        exit 3
    fi
    echo
fi

if [ "$encoding" != "" ]; then
    encoding=" TEMPLATE template0 ENCODING '$encoding'"
fi

query="DROP DATABASE $dbname; CREATE DATABASE $dbname$encoding;" 
echo $query

if [ $pretend -eq 0 ]; then
    echo $query | sudo -u $pguser psql
    [ $? -eq 0 ] || exit
fi

echo "Importing dump: $sqlfile"
if [ $pretend -eq 0 ]; then
    $filter $sqlfile | sed "s/OWNER TO.*/OWNER TO $role;/g" | sudo -u $pguser psql $dbname
fi
