#!/bin/bash
# vim: set tabstop=4 shiftwidth=4 expandtab cindent:
# LICENSE: MIT, see LICENSE file

function usage 
{
    echo 'Symfony 2.1 Doctrine+SQL migrations'
    echo "Usage: $0 -d dbname [-b beforedir] [-a afterdir] [-c console] [-p]"
    echo '    -d database name'
    echo '    -b before doctrine SQL script'
    echo '    -a after  doctrine SQL script'
    echo '    -c console path'
    echo '    -p pretend'
    exit 1
}

function loaddir
{
    if [ -d "$1" ]; then
        echo "Loading SQL scripts from $1"
        for file in $1/*.sql; do
            if [ -f "$file" ]; then
                if [ $3 -eq 0 ]; then
                    echo "Loading $file"
                    cat $file | sudo -u $pguser psql $2
                else
                    echo "Not loading $file"
                fi
            fi
        done
    fi
}

# Default values
wwwuser='www-data'
export pguser='postgres'
pretend=0
filter='cat'

while getopts "a:b:c:d:p" option
do
    case $option in
        a ) after="$OPTARG";;
        b ) before="$OPTARG";;
        c ) console="$OPTARG";;
        d ) dbname="$OPTARG";;
        p ) pretend=1;;
    esac 
done

if [ "$dbname" = "" ]; then
    usage
fi

loaddir "$before" "$dbname" $pretend

if [ -f "$console" ]; then
    if [ $pretend -eq 0 ]; then
        options='--no-interaction'
    else
        options='--dry-run'
    fi

    sudo -u $wwwuser "$console" doctrine:migrations:migrate $options
fi

loaddir "$after" "$dbname" $pretend
