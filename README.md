# dbellettini's migration scripts
This is a set of tools I use for migrating data between servers

## License
These bash scripts are released under MIT license see [LICENSE](https://github.com/dbellettini/migration-scripts/blob/master/LICENSE)
